require 'rails_helper'

RSpec.describe ArrayProcessorController, type: :controller do

  describe '#read_json_file' do
    it 'should handle exception while opening the file gracefully by returning nil' do
      allow(File).to receive(:read).and_raise('There was an error while opening the file.')
      expect(controller.read_json_file('a.json')).to be_nil
    end

    it 'should return the parsed json from the file' do
      allow(File).to receive(:read).and_return([1,2,3,4].to_json)
      expect(controller.read_json_file('a.json')).to eq([1,2,3,4])
    end

  end

  describe '#add_json_arrays' do

    it 'should return the sum of 2 arrays' do
      allow(controller).to receive(:read_json_file).with('a.json').and_return([1,2,3,4,5])
      allow(controller).to receive(:read_json_file).with('b.json').and_return([6,7,8,9,10])
      expect(controller.add_json_arrays).to eq([7, 9, 11, 13, 15])
    end

    it 'should return nil if provided blank arrays' do
      allow(controller).to receive(:read_json_file).and_return([])
      expect(controller.add_json_arrays).to be_nil
    end

  end

  describe '#process_arrays_even_odd' do

    it 'should return nil since the files are empty' do
      allow(controller).to receive(:read_json_file).with('a.json').and_return([])
      allow(controller).to receive(:read_json_file).with('b.json').and_return([])
      allow(controller).to receive(:read_json_file).with('c.json').and_return([])
      expect(controller.process_arrays_even_odd).to be_nil
    end

    it 'should return the sum of odd numbers in the 3rd file since the result is odd' do
      allow(controller).to receive(:read_json_file).with('a.json').and_return([1,2,3,4,5])
      allow(controller).to receive(:read_json_file).with('b.json').and_return([1,2,3,4,5])
      allow(controller).to receive(:read_json_file).with('c.json').and_return([1,2,3,4,5])
      expect(controller.process_arrays_even_odd).to eq(9)
    end

    it 'should return the array of even numbers in the second file if the result is even' do
      allow(controller).to receive(:read_json_file).with('a.json').and_return([2,2,3,4,5])
      allow(controller).to receive(:read_json_file).with('b.json').and_return([1,2,3,4,5])
      allow(controller).to receive(:read_json_file).with('c.json').and_return([1,2,3,4,5])
      expect(controller.process_arrays_even_odd).to eq([2,4])
    end
  end

  describe '#process_even' do
    it 'should return nil since the file is empty' do
      allow(controller).to receive(:read_json_file).with('b.json').and_return([])
      expect(controller.process_even).to be_nil
    end

    it 'should return even numbers in an array' do
      allow(controller).to receive(:read_json_file).with('b.json').and_return([1,2,3,4,5])
      expect(controller.process_even).to eq([2,4])
    end
  end

  describe '#process_odd' do
    it 'should return nil since the file is empty' do
      allow(controller).to receive(:read_json_file).with('c.json').and_return([])
      expect(controller.process_odd).to be_nil
    end

    it 'should return sum of odd numbers' do
      allow(controller).to receive(:read_json_file).with('c.json').and_return([1,2,3,4,5])
      expect(controller.process_odd).to eq(9)
    end
  end

end
