require 'rails_helper'

RSpec.describe CacheController, type: :controller do

  describe '#LRU strategy' do
    it 'should handle nils' do
      expect(controller.get('a')).to be_nil
    end

    it 'should evict the least recently used - case 1' do
      controller.add('a','a')
      controller.add('b','b')
      expect(controller.get('a')).to eq('a')
      expect(controller.get('b')).to eq('b')

      controller.add('c','c')

      expect(controller.get('c')).to eq('c')
      expect(controller.get('b')).to eq('b')
      expect(controller.get('a')).to be_nil
    end

    it 'should evict the least recently used - case 2' do
      controller.add('a','a')
      controller.add('b','b')
      expect(controller.get('a')).to eq('a')
      expect(controller.get('b')).to eq('b')


      controller.add('a','a')
      controller.add('c','c')

      expect(controller.get('c')).to eq('c')
      expect(controller.get('b')).to be_nil
      expect(controller.get('a')).to eq('a')
    end


    it 'should evict the least recently used - case 3' do
      controller.add('a','a')
      controller.add('b','b')
      expect(controller.get('a')).to eq('a')
      expect(controller.get('b')).to eq('b')


      controller.add('a','a')
      controller.add('b','b')
      controller.add('c','c')

      expect(controller.get('c')).to eq('c')
      expect(controller.get('b')).to eq('b')
      expect(controller.get('a')).to be_nil
    end


    it 'should evict the least recently used - case 4' do
      controller.add('a','a')
      controller.add('b','b')
      expect(controller.get('a')).to eq('a')
      expect(controller.get('b')).to eq('b')


      controller.add('a','a')
      controller.add('c','c')
      controller.add('b','b')

      expect(controller.get('c')).to eq('c')
      expect(controller.get('b')).to eq('b')
      expect(controller.get('a')).to be_nil
    end
  end

  describe '#Random strategy' do
    it 'should handle nils' do
      expect(controller.get('a')).to be_nil
    end

    it 'should evict a random one' do
      c = CacheController.new(2,'random')
      c.add('a','a')
      c.add('b','b')
      expect(c.get('a')).to eq('a')
      expect(c.get('b')).to eq('b')
      c.add('c','c')

      expect(c.size).to eq(2)
    end
  end

end
