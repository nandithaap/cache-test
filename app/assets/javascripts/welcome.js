var Civitas = Civitas || {};
Civitas.googleMap = (function($){
    this.displayMap = function() {

        var geocoder = new google.maps.Geocoder();
        var address = "100 Congress, Austin, TX 78701";

        geocoder.geocode( { 'address': address}, function(results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                 latitude = results[0].geometry.location.lat();
                 longitude = results[0].geometry.location.lng();
                var latlng = new google.maps.LatLng(latitude,  longitude);

                var options = {
                    zoom: 8,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapTypeControl: false
                };

                // init map
                var map = new google.maps.Map(document.getElementById('map_canvas'), options);

                // init markers
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    title: 'Civitas Learning'
                });

                // info blurb
                (function(marker) {
                    // add click event
                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow = new google.maps.InfoWindow({
                            content: 'Civitas Learning!! They are an awesome company!!'
                        });
                        infowindow.open(map, marker);
                    });
                })(marker);

            }
        });

    };

    return {
        civitasMap: displayMap
    };

})(jQuery);

$(function() {

    Civitas.googleMap.civitasMap();
});