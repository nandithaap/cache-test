class CacheController < ApplicationController
  attr_accessor :max_size

  def initialize(max_size=2, eviction_strategy='lru' )
    @data = {}
    @max_size = max_size

    if eviction_strategy == 'lru'
      class << self; include LruCacher; end
      initialize_tail_head
    else
      class << self; include RandomCacher end
    end
  end

  def size
    @data.size
  end

  def exists(key)
    @data && @data[key]
  end

end