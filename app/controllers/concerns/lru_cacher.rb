module LruCacher

def self.included(base)
  base.class_eval do
    def initialize_tail_head
      @head = LruCacher::Node.new
      @tail = front(LruCacher::Node.new)
    end
  end
end

def add(key, value)
  n = @data[key]

  unless n
    if size == max_size
      n = delete_oldest
      n.key = key
      n.value = value
    else
      n = Node.new key, value
    end
    @data[key] = n
  end

  front(n).value = value #move to front
  end

  def get(key)
    n = @data[key]
    if n
      front(n).value # if key already in the map, update its usage.
    else
      @data[key] = n
    end
  end


  def delete_oldest
    n = @tail.prev_node
    raise "Cannot delete from empty hash" if @head.equal? n
    remove_node n
  end

  def front(node)
    node.insert_after(@head)
  end

  def remove_node(node)
    n = @data.delete(node.key)
    n.unlink
    n
  end


  Node = Struct.new :key, :value, :priority, :prev_node, :next_node do
    def unlink
      prev_node.next_node = next_node if prev_node
      next_node.prev_node = prev_node if next_node
      self.next_node = self.prev_node = nil
      self
    end

    def insert_after(node)
      raise 'Cannot insert after self' if equal? node
      return self if node.next_node.equal? self
      unlink

      self.next_node = node.next_node
      self.prev_node = node

      node.next_node.prev_node = self if node.next_node
      node.next_node = self

      self
    end
  end

end