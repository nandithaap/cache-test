module RandomCacher

  def add(key, value)
    if size == max_size
      random_key = @data.keys.sample(1).first
      @data.except!(random_key)
    end

    @data[key] = value
  end

  def get(key)
    @data[key]
  end



end