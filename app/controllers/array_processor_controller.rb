class ArrayProcessorController < ApplicationController

  def add_json_arrays
    arr_a = read_json_file('a.json')
    arr_b = read_json_file('b.json')
    [arr_a,arr_b].transpose.map{|elems| elems.sum} if (arr_a.present? && arr_b.present?)
  end

  def process_arrays_even_odd
    first_arr = read_json_file('a.json')
    return nil unless first_arr.present?

    if first_arr.sum.even?
      process_even
    else
      process_odd
    end
  end


  def read_json_file(filename)
    res = nil
    begin
      file = File.read("#{Rails.root}/#{filename}")
      res = JSON.parse(file)
    rescue Exception => e #catch all for any errors raised by reading or parsing the Json file.
      #no-op
    end
    res
  end

  def process_even
    arr = read_json_file('b.json')
    arr.select{|elem| elem.even?} if arr.present?
  end

  def process_odd
    arr = read_json_file('c.json')
    arr.select{|elem| elem.odd?}.sum if arr.present?
  end

end
